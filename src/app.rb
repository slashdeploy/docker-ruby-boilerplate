$stdout.sync = true
$stderr.sync = true

require 'bundler/setup'

# :nodoc:
class Application
  def call(_env)
    [
      200,
      { 'Content-Type' => 'text/plain' },
      ['Hello World']
    ]
  end
end
