require_relative './test_helper'

class ApplicationTest < MiniTest::Test
  include Rack::Test::Methods

  attr_reader :app

  def setup
    @app = Application.new
  end

  def test_root_responds_with_200
    get '/'

    assert_equal 200, last_response.status
  end
end
